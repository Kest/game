package sample.model;

public class Block {
    private boolean isMobile;
    private String color;

    private int x;
    private int y;

    public Block(int x, int y) {
        isMobile = false;
        this.x = x;
        this.y = y;
    }

    public Block(String color, int x, int y) {
        isMobile = true;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public boolean isMobile() {
        return isMobile;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }


}
