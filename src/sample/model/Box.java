package sample.model;

public class Box {
    Block block;

    int x;
    int y;

    public Box(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}
