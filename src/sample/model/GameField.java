package sample.model;

import java.util.ArrayList;
import java.util.List;

public class GameField {
    Box[][] boxes = new Box[5][5];

    public List<Box> getAvaibleBoxes(int x, int y){
        List<Box> boxList = new ArrayList<>();
        if ((x + 1) < 5) {
            if (boxes[x + 1][y] == null) {
                boxList.add(new Box(x + 1, y));
            }
        }

        if (x > 0){
            if (boxes[x - 1][y] == null) {
                boxList.add(new Box(x - 1, y));
            }
        }

        if ((y + 1) < 5) {
            if (boxes[x][y + 1] == null) {
                boxList.add(new Box(x, y + 1));
            }
        }

        if (y > 0){
            if (boxes[x][y - 1] == null) {
                boxList.add(new Box(x, y - 1));
            }
        }
        return boxList;

    }

    public boolean changePosition(Box boxStart, Box boxFinish){
        boxes[boxFinish.x][boxFinish.y] = boxes[boxStart.x][boxStart.y];
        boxes[boxStart.x][boxStart.y] = null;
        return true;
    }

    public Box getBox(int x, int y){
        return boxes[x][y];
    }

    public static void main(String[] args) {
        GameField gameField = new GameField();
        gameField.boxes[0][0] = new Box(0, 0);
        List<Box> boxList = gameField.getAvaibleBoxes(1, 0);
        gameField.changePosition(gameField.getBox(0, 0), new Box(1, 0));
        System.out.println(gameField.getAvaibleBoxes(1, 0));
    }
}
